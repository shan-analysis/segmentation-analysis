#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Calculate label volumes')
parser.add_argument('-i', '--input', required=True,
                    help='Segmentation filename')
parser.add_argument('-o', '--output', required=True,
                    help='Label volume filename')
parser.add_argument('-s', '--sep', default='\t', help='Delimiter')
parser.add_argument('-r', '--remove', action='store_true', default=False,
                    help='Remove the first label (background)')
args = parser.parse_args()

import pandas as pd
import nibabel as nib
import numpy as np
from collections import OrderedDict

obj = nib.load(args.input)
segmentation = obj.get_data()
labels = np.unique(segmentation).astype(int)
if args.remove:
    labels = labels[1:]
voxel_size = np.prod(obj.header.get_zooms())

volumes = OrderedDict()
for label in labels:
    mask = segmentation == label
    volume = np.sum(mask) * voxel_size
    volumes[label] = [volume]

df = pd.DataFrame(volumes)
df.to_csv(args.output, sep=args.sep, index=False)
